import React from "react";
import {Header, Footer, Main, Flex} from "../components";

const Home = () => {
  return (
    <Flex
      height="100vh"
      direction="column"
      justify="space-between"
      align="center"
      margin="0px">
      <Header />
      <Main />
      <Footer />
    </Flex>
  );
};
export {Home};
