import React from "react";
import {Router} from "@reach/router";
import {Home, Dashboard, Profile} from "./MainContent";

const App = () => {
  return (
    <>
      <Router>
        <Home path="/" />
        <Dashboard path="dashboard" />
        <Profile path="profile" />
      </Router>
    </>
  );
};

export default App;
