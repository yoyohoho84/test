import React from "react";
import styled from "styled-components";

const StyledFooter = styled.footer`
  font-size: 21px;
  color: yellow;
  margin-bottom: 20px;
`;

const Footer = () => {
  return <StyledFooter>Footer</StyledFooter>;
};
export {Footer};
