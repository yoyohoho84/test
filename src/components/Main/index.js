import React, {useState} from "react";
import styled from "styled-components";
import {Flex} from "../../components";

const StyledMain = styled.div`
  font-size: 34px;
  color: green;
`;

const StyledMainButton = styled.button`
  display: inline-block;
  color: palevioletred;
  font-size: 1em;
  margin: 1em;
  padding: 0.25em 1em;
  border: 2px solid palevioletred;
  border-radius: 7px;
  display: block;
`;

const Main = () => {
  const [count, setCount] = useState(0);

  return (
    <StyledMain>
      <Flex direction="row" justify="center" align="center">
        {count}
      </Flex>
      <Flex direction="row" justify="space-between" align="center" margin="10px">
        <StyledMainButton onClick={() => setCount((prevState) => prevState + 1)}>
          +
        </StyledMainButton>
        <StyledMainButton onClick={() => setCount((prevState) => prevState - 1)}>
          -
        </StyledMainButton>
      </Flex>
    </StyledMain>
  );
};
export {Main};
