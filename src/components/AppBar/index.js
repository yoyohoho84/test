import React from "react";
import styled from "styled-components";
import {Flex} from "../../components";
import {Link} from "@reach/router";

// const Link = ({className, children}) => <a className={className}>{children}</a>;

const StyledLink = styled(Link)`
  color: palevioletred;
  font-weight: bold;
  padding: 10px;
  text-decoration: none;
`;

const AppBar = () => {
  return (
    <Flex>
      <StyledLink to="/">Home</StyledLink>
      <StyledLink to="/dashboard">Dashboard</StyledLink>
      <StyledLink to="/profile">Profile</StyledLink>
    </Flex>
  );
};
export {AppBar};
