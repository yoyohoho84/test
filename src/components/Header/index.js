import React from "react";
import styled from "styled-components";
import {AppBar} from "../../components";

const StyledHeader = styled.header`
  font-size: 55px;
`;

const Header = () => {
  return (
    <StyledHeader>
      <AppBar />
    </StyledHeader>
  );
};
export {Header};
